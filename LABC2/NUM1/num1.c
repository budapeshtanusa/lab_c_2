/*
Функция получает линейный массив целых, находит в нем последовательности 
подряд возрастающих значений и возвращает их в динамическом массиве 
указателей на линейные массивы (аналог двумерного массива). В каждом из 
линейных динамических массивов содержится копия возрастающей 
последовательности, начиная с индекса 1, а под индексом 0 содержится его 
длина. Невозрастающие значения включаются в отдельный массив, добавляемый 
в конец (или начало) массива указателей.
*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
int **function(int* mas_num,int in_mas_len){
	int **big_mas = calloc(100,sizeof(int*));//Результирующий массив
	int *temp_mas = calloc(100,sizeof(int));//Текущая последовательность
	int prev=-1;//Предыдущий элемент последовательности
	int len=0;//Длина текущей последовательности
	int count_array=0;//Колличество последовательностей в результирующем
			  //массиве
	int *wrong_num = calloc(100,sizeof(int));//массив невозрастающий послед
	int wrong_count=0;//Колличество элементов в массиве невозраст послед
	for(int i = 0; i < in_mas_len; i++) {
		//Если первый элемент добавляем его к последовательности
		if(prev == -1){
			temp_mas[len+1]=mas_num[i];
			len++;
			prev=mas_num[i];
			continue;
		}
		//Если текущий элемент в последовательности дописываем его в
		//массив
		if(mas_num[i]>prev) {
			temp_mas[len+1]=mas_num[i];
			len++;
		}
		//Если текущий элемент не в последовательности
		//то проверяеряем предыдущую на валидность и создаем новую 
		//последовательность
		else {
			//Если предыдущая послед валидна то добавляем ее
			//в результирующай массив
		       	if(len>1){
				temp_mas[0]=len;
				big_mas[count_array]=temp_mas;
				count_array++;
				temp_mas=calloc(100,sizeof(int));
			} else {
				for(int i=0;i<len;i++){
					wrong_num[wrong_count+1]=temp_mas[i+1];
					wrong_count++;
				}
			}
			len=0;
			temp_mas[len+1]=mas_num[i];
			len++;
		}
		prev=mas_num[i];
	}
	if(len>1){
		temp_mas[0]=len;
		big_mas[count_array]=temp_mas;
		count_array++;
	}
	else {
		for(int i=0;i<len;i++){
			wrong_num[wrong_count+1]=temp_mas[i+1];
			wrong_count++;
		}
		free(temp_mas);
	}
	if(wrong_count>0){
		wrong_num[0]=wrong_count;	
		big_mas[count_array]=wrong_num;
	}
	return big_mas;
}

int char_to_int(char a){
	if (a<'0' || a>'9'){
		return -1;
	}
	return a-'0';
}

int main() {
	char in_mas[20];
	memset(in_mas,0,sizeof(in_mas));
	int mas_num[20];
	memset(mas_num,0,sizeof(mas_num));
	printf("Insert string: ");
	scanf("%20s",in_mas);
	for(int i=0;i<strlen(in_mas);i++){
		mas_num[i]=char_to_int(in_mas[i]);
	}
	int** big_mas=function(mas_num,strlen(in_mas));
	for(int i=0;big_mas[i]!=NULL;i++){
		int len=big_mas[i][0];
		for(int j=0;j<=len;j++){
			printf("%d ",big_mas[i][j]);
		}
		printf("\n");
	}
	for(int i=0;big_mas[i]!=NULL;i++){
		free(big_mas[i]);
	}
	free(big_mas);
}
