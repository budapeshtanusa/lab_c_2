#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int N=2;

struct stack {
	int** stack_mas;
	int strok;
	int stolb;
};

void help () {
	printf("1) Push\n");
	printf("2) Pop\n");
	printf("3) Top\n");
	printf("4) IsEmpty\n");
	printf("0) Exit\n");
}

void push(struct stack* s,int value){
	if(s->stack_mas[s->strok]==NULL){
		s->stack_mas[s->strok]=calloc(N,sizeof(int));
	}
	s->stack_mas[s->strok][s->stolb]=value;
	s->stolb++;
	if(s->stolb>=N){
		s->strok++;
		s->stack_mas[s->strok]=calloc(N,sizeof(int));
		s->stolb=0;
	}
}

int pop(struct stack* s){
	if(s->strok==0 && s->stolb==0){
		printf("Stack is empty\n");
		return -1;
	}
	s->stolb--;
	if(s->stolb<0){
		free(s->stack_mas[s->strok]);
		s->stack_mas[s->strok]=NULL;
		s->strok--;
		s->stolb=N-1;
	}
	int value=s->stack_mas[s->strok][s->stolb];
	s->stack_mas[s->strok][s->stolb]=0;
	if(s->strok==0 && s->stolb==0){
		free(s->stack_mas[s->strok]);
		s->stack_mas[s->strok]=NULL;
	}
	return value;
}

int top (struct stack* s){
	if(s->strok==0 && s->stolb==0){
		printf("Stack is empty\n");
		return -1;
	}
	int top_val=s->stack_mas[s->strok][s->stolb-1];
	return top_val;
}

int isempty(struct stack* s){
	if(s->strok==0 && s->stolb==0){
		printf("Stack is empty\n");
		return -1;
	}
	printf("Stack is not empty\n");
	
}


int main(){
	int m;
	int val;
	struct stack s;
	memset(&s,0,sizeof(struct stack));
	s.stack_mas=calloc(100,sizeof(int*));

	while (1){
		help();
		scanf("%d",&m);
		switch(m) {
			case 1:
				printf("Input value: ");
				scanf("%d",&val);
				push(&s,val);
				break;
			case 2:
				printf("Last value: %d\n",pop(&s));
				break;
			case 3:
				printf("Top value: %d\n",top(&s));
				break;
			case 4:
				isempty(&s);
				break;
			case 0:
				return 0; 
		}
	}
}
