/*
Стек моделируется при помощи динамического массива указателей на линейные массивы
размерности N целых. Указатель стека – два индекса – в массиве указателей и линейном 
массиве. В операции push при переполнении текущего линейного массива в массив 
указателей добавляется новый, если операция pop переходит к предыдущему массиву, то 
текущий утилизуется.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
const int N=3;

struct stack{
	int** big_mas;
	int row;
	int column;
};

int pop(struct stack* stack_t){
	if(stack_t->row==0 && stack_t->column==0 ){
		printf("Stack is empty\n");
		return -1;
	}
	stack_t->column--;
	if(stack_t->column<0){
		free(stack_t->big_mas[stack_t->row]);
		stack_t->big_mas[stack_t->row]=NULL;
		stack_t->row--;
		stack_t->column=N-1;
	}
	int value=stack_t->big_mas[stack_t->row][stack_t->column];
	stack_t->big_mas[stack_t->row][stack_t->column]=0;
	if(stack_t->row==0 && stack_t->column==0){
		free(stack_t->big_mas[stack_t->row]);
		stack_t->big_mas[stack_t->row]=NULL;
	}
	return value;
}

void push(struct stack* stack_t,int value){
	if(stack_t->big_mas[stack_t->row]==NULL){
		stack_t->big_mas[stack_t->row]=calloc(N,sizeof(int));
	}
	stack_t->big_mas[stack_t->row][stack_t->column]=value;
	stack_t->column++;
	if(stack_t->column>=N){
		stack_t->row++;
		stack_t->big_mas[stack_t->row]=calloc(N,sizeof(int));
		stack_t->column=0;
	}
}

void print(struct stack* stack_t){
	for(int i=0;i<=stack_t->row;i++){
		if(stack_t->big_mas[i]==NULL){
			continue;
		}
		for(int j=0;j<N;j++){
			printf("big_mas[%d][%d]=%d \n",i,j,stack_t->big_mas[i][j]);
		}
	}
}

void help() {
	printf("1) Add symbol at stack\n");
	printf("2) Get symbol from stack\n");
	printf("3) Show stack\n");
	printf("0) Exit from menu\n");
}

int main(){
	int m;
	int val;
	struct stack stack_t;
	memset(&stack_t,0,sizeof(struct stack));
	stack_t.big_mas=calloc(100,sizeof(int*));
	while (1){
		help();
		scanf("%d",&m);
		switch(m) {
			case 1: 
				printf("Input value: ");
				scanf("%d",&val);
				push(&stack_t,val);
				break;
			case 2:
				printf("Last value: %d\n",pop(&stack_t));
				
			        break;
			case 3: 
				print(&stack_t);
				break;
			case 0:
				return 0;
		}	
		
	}
}



